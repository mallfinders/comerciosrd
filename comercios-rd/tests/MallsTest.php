<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Repositories\MallsRepository;

class MallsTest extends TestCase
{
    use DatabaseTransactions;
    
    /**
     * Mock data
     * @var array
     */
    private $mallMock = array(
    	'name'=>'Comercio Test 1',
   		'description'=>'Testing add new mall in repository',
   		'lat' => '18.48372',
   		'lng' => '-69.9420119'
   		);
    
    /**
     * use to edit an existent mock mall
     * @var array
     */
    private $mallEditedMock = array(
    	'name'=>'Comercio Test 1',
   		'description'=>'Testing',
   		'lat' => '18.4835672',
   		'lng' => '-69.9420119'
   		);

    /**
     * use to test mock data
     * @var array
     */
    private $mallMockId = array(
    	'id' => 9999999999);
   
    /**
     * test Adding a New Mall In Repository 
     */
    public function testAddNewMallInRepository()
    {   
   		MallsRepository::create($this->mallMock);
    	$mall = MallsRepository::all()->last();
    	$this->assertEquals('Comercio Test 1', $mall->name );
    }

    /**
     * test Get Mall List In Repository
     */
 		public function testGetMallListInRepository()
    {   
   		$malls= MallsRepository::all();
    	$this->assertEquals(0 , count($malls) );
    } 

    /**
     * test Editing Nonexistent Mall In Repository 
     */
    public function testEditNonexistentMallInRepository(){
    	$response = MallsRepository::update($this->mallMockId['id'], $this->mallEditedMock);
    		$this->assertEquals(0 ,$response);
    }   

    /**
     * [test Deleting Nonexistent Mall In Repository 
     */
    public function testDeleteNonexistentMallInRepository(){
    	$response = MallsRepository::delete($this->mallMockId['id']);
    		$this->assertEquals(0 ,$response);
    }  

    /**
     * [test Deleting Mall In Repository 
     */
    public function testDeleteMallInRepository(){
			$newMall = MallsRepository::create($this->mallMock);
			$mall = MallsRepository::all()->last();
    	$response = MallsRepository::delete($mall->id);
    		$this->assertEquals(1 ,$response);
    }

    /**
     * [test Editing Mall In Repository 
     */
		public function testEditMallInRepository(){
			$newMall = MallsRepository::create($this->mallMock);
			$mall = MallsRepository::all()->last();
      $editedMall = MallsRepository::update($mall->id , $this->mallEditedMock);
			$mall = MallsRepository::all()->last();
      
    	$this->assertEquals('Comercio Test 1', $mall->name );
    	$this->assertEquals('Testing', $mall->description );
    	$this->assertEquals('18.4835672', $mall->lat);
    	$this->assertEquals('-69.9420119', $mall->lng);
		}   

		/**
		 * [test Getting Mall Detail In Repository 
		 */
		public function testGetMallDetailInRepository()
    {   
   		MallsRepository::create($this->mallEditedMock);
    	$mall = MallsRepository::all()->last();
    	$mall = MallsRepository::find($mall->id);

    	$this->assertEquals('Comercio Test 1', $mall->name );
    	$this->assertEquals('Testing', $mall->description );
    	$this->assertEquals('18.4835672', $mall->lat);
    	$this->assertEquals('-69.9420119', $mall->lng);
    }

}
