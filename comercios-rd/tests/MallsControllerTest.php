<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use App\Repositories\MallsRepository;

class MallsControllerTest extends TestCase
{
	use DatabaseTransactions;

		/**
		 * Mock data
		 * @var array
		 */
    private $mallMock = array(
    	'name'=>'Comercio Test 1',
   		'description'=>'Testing add new mall in repository',
   		'lat' => '18.48372',
   		'lng' => '-69.9420119'
   		);

    /**
     * test Adding a New Mall checking its fields
     */
    public function testAddNewMall()
    {
    	  $response = $this->call('POST', '/malls', $this->mallMock)
    	  ;
    	 	$mall = MallsRepository::all()->last();
    		$this->assertEquals('Comercio Test 1', $mall->name );
    		$this->assertEquals('Testing add new mall in repository', $mall->description );
    		$this->assertEquals('18.48372', $mall->lat );
    		$this->assertEquals('-69.9420119', $mall->lng );
    	  $this->assertRedirectedTo('/malls');
    }  

    /**
     * test Mall List 
     */
    public function testGetMallList()
    {
    	$mall1 = array('name' => 'Agora Mall',
    		'description' => 'Agora Mall test',
    		'lat' => -69.3453,
    		'long' => 80.99384);
    	$createdMall1 = MallsRepository::create($mall1);
    	$createdMalls2 = MallsRepository::create($this->mallMock);
    	$this->visit('/malls')
    	->see('Agora Mall')
    	->see('Comercio Test 1');
    }
}
