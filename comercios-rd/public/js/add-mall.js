$(function(){
	var selectedMarker = null;

	var map = new GMaps({
		el: '#map',
		lat: 18.48372,
		lng:-69.9420119,
		click: addMarker
	});

	function addMarker(event) {
		if (!selectedMarker) {
			selectedMarker = map.addMarker({
			  lat: event.latLng.lat(),
			  lng: event.latLng.lng()
			});
		} else {
			selectedMarker.setPosition(event.latLng);
		}
		$('#map-latitude').val(event.latLng.lat());
		$('#map-longitude').val(event.latLng.lng());
	}

});
