$(function(){
	var map = new GMaps({
		el: '#map',
		 lat: $('#map').attr('data-lat'),
		 lng: $('#map').attr('data-lng')
	});

	map.addMarker({
	  lat: $('#map').attr('data-lat'),
	  lng: $('#map').attr('data-lng'),
	});
});
