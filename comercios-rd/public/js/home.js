$(function(){
	console.log(listMalls);
	var polygonPaths = [];
	var polygon = null;

	var map = new GMaps({
		el: '#map',
		lat: 18.48372,
		lng:-69.9420119
	});

	listMalls.forEach(function(mall){
		map.addMarker(mall);
	});
	map.fitZoom();

	$('.malls-name-checkbox').on('change', function(){
		polygonPaths = [];
		$('.malls-name-checkbox:checked').each(function(){
			polygonPaths.push(new google.maps.LatLng(
				$(this).attr('data-lat'),
				$(this).attr('data-lng')
				));
		});
	updateMapPolygon();
  $('#area').html( getArea().toFixed(2) + ' mts2');
	});

	function updateMapPolygon() {
      if (!polygon) {
        polygon = map.drawPolygon({
          paths: polygonPaths,
          strokeColor: '#BBD8E9',
          strokeOpacity: 1,
          strokeWeight: 3,
          fillColor: '#BBD8E9',
          fillOpacity: 0.6
        });
      } else {
        polygon.setPaths(polygonPaths);
      }
    }

    function getArea() {
      if (polygonPaths.length > 2) {
        return google.maps.geometry.spherical.computeArea(polygon.getPath());
      }
      return 0;
    }

});
