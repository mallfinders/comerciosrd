<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mall extends Model
{
  protected $fillable = ['code','name','description','lat','lng'];
}
