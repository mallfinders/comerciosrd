<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Mall;
use App\Http\Requests;

class PagesController extends Controller
{
    function index(){
    $malls= Mall::all();
    return view('home',compact('malls'));
    }
}
