<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\Repositories\MallsRepository;

class MallsController extends Controller
{
   function index()
   {
   	$mallList= MallsRepository::all()->toArray();
     return view('malls.mall-list', compact('mallList'));
   }

   function store(Request $request)
   {
   	$mall = $request->input();
   	$this->validate($request,[
   		'name'=>'required',
   		'description'=>'required',
   		'lat' => 'required',
   		'lng' => 'required'
   	]);
   	MallsRepository::create($mall);
   	return redirect('/malls')->with('status', 'El Comercio ha sido agregado de forma exitosa!');
   }

   function create(Request $request)
   {
   	return view('malls.add-mall');
   }

   function show($id)
   {
   	$mall = MallsRepository::find($id)->toArray();
		return view('malls.mall-detail',compact('mall'));
   }

   function update($id, Request $request)
   {
   	MallsRepository::update($id,$request->input()['data']);
   	return redirect('/malls')->with('status', 'El Comercio ha sido Modificado de forma exitosa!');
   }

   function destroy($id)
   {
   	MallsRepository::delete($id);
   	return redirect('/malls')->with('status', 'Comercio eliminado de forma exitosa!');
   }

   function edit($id, Request $request)
   {
   	$mall = MallsRepository::find($id)->toArray();
   	return view('malls.edit-mall',compact('mall'));
   }
 }