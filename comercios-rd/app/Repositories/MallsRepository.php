<?php

namespace App\Repositories;
use App\Mall;
class MallsRepository{

	/**
	 * Get all malls
	 * @return object of malls
	 */
	public static function  all()
	{
		return $mallList= Mall::all();
	}

	/**
	 * Create a new mall
	 * @param  array $mall 
	 * @return boolean 0 if fail, 1 if was succed  
	 */
	public static function  create($mall)
	{
		$code = array('code' => str_random(10) );
		return Mall::create(array_merge($code, $mall));
	}

	/**
	 * find a mall
	 * @param  int $id mall id
	 */
	public static function  find($id)
	{
		return $mall = Mall::findOrFail($id);
	}

	/**
	 * update an existent mall
	 * @param  int $id   
	 * @param  array $data 
	 */
	public static function  update($id,$data)
	{
		return Mall::where('id', $id)
		->update($data);   
	}

	/**
	 * Delete an existent mall
	 * @param  int $id 
	 */
	public static function  delete($id)
	{
		return Mall::where('id', $id)->delete();
	}
}  
//EOF