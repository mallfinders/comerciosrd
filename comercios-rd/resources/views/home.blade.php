@section('body-class', 'has-header')
@extends('layouts.app')
@section('content')

<div class="container">
	<div id="carousel-example-generic" class="carousel slide carousel-container" data-ride="carousel">
	  <!-- Indicators -->
	  <ol class="carousel-indicators">
			  @foreach ($malls as $key => $mall)
			    <li data-target="#carousel-example-generic" data-slide-to="$key" class="{{$key==1 ? 'active': '' }}"></li>
			  @endforeach
	  </ol>

	  <!-- Wrapper for slides -->
	  <div class="carousel-inner carousel-items" role="listbox">

			  @foreach ($malls as $key => $mall)
			  <div class="item text-center {{$key==1 ? 'active': '' }}">
			  	<div class="col-md-6">
			  		<a href="/malls/{{$mall['id']}}">
			  			<h1 class="text-center add-mall-text">{{$mall['name']}}</h1>
			  		</a>
			  	</div>
			  	<div class="col-md-6">
			  		<img src="images/lupa_mall.png" alt="Blue mall">
			  	</div>
			  </div>
			  @endforeach
	  	{{-- Add a mall --}}
	  	<div class="item text-center">
	  		<div class="col-md-6">
	  			<a href="/malls/create">
	  			<h1 class="text-center add-mall-text">Agrega un comercio</h1>
	  			</a>
	  		</div>
	  		<div class="col-md-6">
	  			<a href="/malls/create">
	  			<img src="images/shopping_basket_add_256.png" alt="">
	  			</a>
	  			</div>
	  	</div>
	  	{{-- Watch mall list--}}
	  	
	  </div>

	  <!-- Controls -->
	  <a class="left carousel-control" href="#carousel-example-generic" role="button" data-slide="prev">
	    <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
	    <span class="sr-only">Anterior</span>
	  </a>
	  <a class="right carousel-control" href="#carousel-example-generic" role="button" data-slide="next">
	    <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
	    <span class="sr-only">Siguiente</span>
	  </a>
	</div>
	<div class="map-notice">

	</div>
	<div class="map-container">
		<div class="row">
			<div class="col-md-9">
				<div class="map" id="map">
				</div>
			</div>
			<div class="col-md-3">
				Comercios disponibles
				 <h3>Area: <span id="area">0</span></h3>
				  @foreach ($malls as $key => $mall)
					<p>
						<label>
							<input class="malls-name-checkbox" type="checkbox" data-lat="{{$mall['lat']}}" data-lng="{{$mall['lng']}}">
							 {{$mall['name']}}
						</label>
					</p>
			  	@endforeach

			</div>

		</div>
	</div>
</div>
@stop

@section('scripts')
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyD68Quv1WO8KJDD50bILDDqaKqKR__UMZY" type="text/javascript"></script>
<script type="text/javascript" src="/js/gmaps.js"></script>
<script type="text/javascript">
	var listMalls = {!!json_encode($malls)!!}; 
</script>
<script type="text/javascript" src="/js/home.js"></script>
@stop