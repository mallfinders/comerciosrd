@extends('layouts.app')
@section('content')
<div class="container">
	<div class="col-sm-3">
		<img class="img-responsive" src="/images/chica-de-compras-04-g.png">
	</div>
	<div class="col-sm-9">
		<h2>Agregar un comercio</h2>
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<form action="/malls" method="POST" class="row">
			{{ csrf_field() }}
			<input class="form-control" id="map-latitude" type="hidden" name="lat">
			<input class="form-control" id="map-longitude" type="hidden" name="lng">
			<div class="col-sm-6">
				<div class="form-group">
					<label>Nombre: </label>
					<input class="form-control" type="text" name="name">
				</div>
				<div class="form-group">
					<label>Descripcion: </label>
					<textarea class="form-control" name="description" rows="8"></textarea>
				</div>
				<button class="cancel-add-mall-btn btn btn-default">Cancelar</button>
				<input type="submit" name="" value="Agregar Comercio" class="btn btn-primary add-mall-btn">
			</div>
			<div class="col-sm-6">
			 <input id="map-search-input" class="controls" type="text" placeholder="Busca tu comercio aqui">
				<div id="map" class="map"></div>
			</div>
		</form>
	</div>
</div>
@stop

@section('scripts')
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyD68Quv1WO8KJDD50bILDDqaKqKR__UMZY" type="text/javascript"></script>
<script type="text/javascript" src="/js/gmaps.js"></script>
<script type="text/javascript" src="/js/add-mall.js"></script>
@stop