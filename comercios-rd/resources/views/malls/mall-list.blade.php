@section('body-class', 'mall-list')
@extends('layouts.app')
@section('content')
<div class=" mall-list-container">
	<div class="mall-list-content">
		<div class="container ">
			<div class="row">
				@if ($mallList)
				@foreach ($mallList as $mall)
				<div class="col-md-3 mall-list-images">
					<div class="mall-options">
						<a href="/malls/{{$mall['id']}}">Ver comercio</a>
						<a href="/malls/{{$mall['id']}}/edit">Editar comercio</a>
						<a href="/malls/{{$mall['id']}}/delete" class="delete-mall">Borrar comercio</a>
					</div>
					<img src="/images/placeholder-basket.png">
					<h4 class="text-center">{{$mall['name']}}</h4>
				</div>
				@endforeach
				@endif
		</div>
	</div>
</div>
@stop
@section('scripts')
<script type="text/javascript" src="/js/mall-list.js"></script>
@stop