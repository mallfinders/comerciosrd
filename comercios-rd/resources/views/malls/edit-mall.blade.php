@extends('layouts.app')
@section('content')
<div class="container">
	<div class="col-sm-3">
		<img class="img-responsive" src="/images/chica-de-compras-04-g.png">
	</div>
	<div class="col-sm-9">
		<h2>Editar {{$mall['name']}}</h2>
		@if (count($errors) > 0)
		<div class="alert alert-danger">
			<ul>
				@foreach ($errors->all() as $error)
				<li>{{ $error }}</li>
				@endforeach
			</ul>
		</div>
		@endif
		<form action="/malls/{{$mall['id']}}" method="POST" class="row">
			{{ csrf_field() }}
			{{ method_field('PUT') }}
			<input class="form-control"  id="map-latitude" value="{{$mall['lat']}}" type="hidden" name="data[lat]">
			<input class="form-control"  id="map-longitude" value="{{$mall['lng']}}" type="hidden" name="data[lng]">
			<div class="col-sm-6">
				<div class="form-group">
					<label>Nombre: </label>
						<input class="form-control" value="{{$mall['name']}}" type="text" name="data[name]">
				</div>
				<div class="form-group">
					<label>Descripcion: </label>
					<textarea class="form-control" name="data[description]" rows="8">{{$mall['description']}}</textarea>
				</div>
				<button class="cancel-add-mall-btn btn btn-default">Cancelar</button>
				<input type="submit" name="" value="Editar Comercio" class="btn btn-primary add-mall-btn">
			</div>
			<div class="col-sm-6">
				<div id="map" class="map" data-lat="{{$mall['lat']}}" data-lng="{{$mall['lng']}}"></div>
			</div>
		</form>
	</div>
</div>
@stop

@section('scripts')
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyD68Quv1WO8KJDD50bILDDqaKqKR__UMZY" type="text/javascript"></script>
<script type="text/javascript" src="/js/gmaps.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
@stop