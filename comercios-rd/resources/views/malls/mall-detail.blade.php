@section('body-class', '')
@extends('layouts.app')
@section('content')

<h1 class="text-center mall-title">{{$mall['name']}}</h1>
<div class="mall-image">
	<img src="/images/shopping-tag.png">
</div>
@if ($mall)
<div class="mall-container container">
	<div class="row">
		<div class="col-md-6">
			<p><strong>Nombre: </strong> <span>{{$mall['name']}}</span></p>
		</div>
		<div class="col-md-6">
			<p>{{ $mall['description'] }}</p>
		</div>
	</div>
</div>
<div class="map-container">
	<div class="map" id="map" data-lat="{{$mall['lat']}}" data-lng="{{$mall['lng']}}">
	</div>
</div>
@endif
@stop
@section('scripts')
<script src="//maps.googleapis.com/maps/api/js?key=AIzaSyD68Quv1WO8KJDD50bILDDqaKqKR__UMZY" type="text/javascript"></script>
<script type="text/javascript" src="/js/gmaps.js"></script>
<script type="text/javascript" src="/js/main.js"></script>
@stop