<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	
		<!-- Fonts  -->	
	  <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.5.0/css/font-awesome.min.css" integrity="sha384-XdYbMnZ/QjLh6iI4ogqCTaIjrFk87ip+ekIjefZch0Y+PvJ8CDYtEs1ipDmPorQ+" crossorigin="anonymous">
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Lato:100,300,400,700">

		<!-- Icons -->

		<!-- Styles -->
		<link rel="stylesheet" type="text/css" href="/assets/bower/bootstrap/dist/css/bootstrap.min.css">
		<link href="/css/style.css" rel="stylesheet" type="text/css">
	<title>Comercios RD</title>

</head>
<body id="app-layout" class="@yield('body-class', '')">
<div class="navbar">
		<div class="header-container container">
			<div class="col-md-6 logo-container">
				<a href="/"><img src="/images/mall-finder-icon3.png"></a>
			</div>
			<div class="col-md-6">
				<ul class="nav navbar-nav navbar-right">
					<li role="presentation" class="active"><a href="/">Inicio</a></li>
					<li role="presentation"><a href="/malls">Listado de comercios</a></li>
					<li role="presentation"><a href="/malls/create">Agregar un comercio</a></li>
				</ul>
			</div>
		</div>	
	</div>
	@if (session('status'))
	<div class="alert alert-success">
		{{ session('status') }}
	</div>
	@endif
	@yield('content')


	<!-- Javascripts -->
	<script type="text/javascript" src="/assets/bower/jquery/dist/jquery.min.js"></script>
	<script type="text/javascript" src="/assets/bower/bootstrap/dist/js/bootstrap.min.js"></script>
	@yield('scripts')
</body>
</html>